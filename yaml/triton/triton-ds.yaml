apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: triton-gpu
  labels:
    k8s-app: triton-gpu
spec:
  selector:
    matchLabels:
      name: triton-gpu
  template:
    metadata:
      labels:
        name: triton-gpu
    spec:
      nodeSelector:
        triton: "enabled"
        smarter.nodetype: "jetson"
      tolerations:
      - key: "smarter.type"
        value: "edge"
        effect: "NoSchedule"
      hostname: triton
      containers:
      - name: triton
        imagePullPolicy: IfNotPresent
        command: ["/triton/bin/tritonserver"]
        args: ["--model-repository", "/triton/model_repository"]
        image: registry.gitlab.com/arm-research/smarter/jetpack-triton:v2.0_ml_latest
        resources:
          limits:
            smarter-devices/nvidia-gpu0: 1
          requests:
            smarter-devices/nvidia-gpu0: 1
        ports:
          - name: http
            containerPort: 8000
            protocol: TCP
          - name: grpc
            containerPort: 8001
            protocol: TCP
          - name: metrics
            containerPort: 8002
            protocol: TCP
        livenessProbe:
          httpGet:
            path: /v2/health/live
            port: http
        readinessProbe:
          initialDelaySeconds: 5
          periodSeconds: 5
          httpGet:
            path: /v2/health/ready
            port: http
        volumeMounts:
          - name: config
            mountPath: /triton/model_repository/ssd_mobilenet_coco/config.pbtxt
            subPath: ssd_mobilenet_coco_config
          - name: config
            mountPath: /triton/model_repository/vggish/config.pbtxt
            subPath: vggish_config
          - name: config
            mountPath: /triton/model_repository/ambient_sound_clf/config.pbtxt
            subPath: ambient_sound_clf_config
      terminationGracePeriodSeconds: 30
      volumes:
        - name: config
          configMap:
            name: triton-config

---

apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: triton-armnn
  labels:
    k8s-app: triton-armnn
spec:
  selector:
    matchLabels:
      name: triton-armnn
  template:
    metadata:
      labels:
        name: triton-armnn
    spec:
      nodeSelector:
        triton: "enabled"
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: smarter.nodetype
                operator: NotIn
                values:
                - jetson
      tolerations:
      - key: "smarter.type"
        value: "edge"
        effect: "NoSchedule"
      hostname: triton
      containers:
      - name: triton
        imagePullPolicy: IfNotPresent
        command: ["/opt/tritonserver/bin/tritonserver"]
        args: ["--model-repository", "/triton/model_repository", "--model-control-mode=none"]
        image: registry.gitlab.com/arm-research/smarter/jetpack-triton:ml_armnn
        ports:
          - name: http
            containerPort: 8000
            protocol: TCP
          - name: grpc
            containerPort: 8001
            protocol: TCP
          - name: metrics
            containerPort: 8002
            protocol: TCP
        livenessProbe:
          httpGet:
            path: /v2/health/live
            port: http
        readinessProbe:
          initialDelaySeconds: 5
          periodSeconds: 5
          httpGet:
            path: /v2/health/ready
            port: http
        volumeMounts:
          - name: config
            mountPath: /triton/model_repository/ssd_mobilenet_coco/config.pbtxt
            subPath: ssd_mobilenet_coco_config
      terminationGracePeriodSeconds: 30
      volumes:
        - name: config
          configMap:
            name: triton-config-armnn