apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: gstreamer-rpi
  labels:
    k8s-app: gstreamer-rpi
spec:
  selector:
    matchLabels:
      name: gstreamer-rpi
  template:
    metadata:
      labels:
        name: gstreamer-rpi
    spec:
      nodeSelector:
        gstreamer: "enabled"
        smarter.nodetype: "raspberrypi4"
      tolerations:
      - key: "smarter.type"
        value: "edge"
        effect: "NoSchedule"
      hostname: gstreamer
      containers:
      - name: gstreamer-videotee
        imagePullPolicy: IfNotPresent
        command: ["/bin/bash"]
        args: ["-c", "gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=640,height=480,framerate=30/1 ! videoconvert ! video/x-raw,format=I420 ! tee name=30raw ! queue ! shmsink socket-path=/tmp/gstreamer/raw-30 shm-size=100000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=1/1 ! shmsink socket-path=/tmp/gstreamer/raw-1 shm-size=20000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=5/1 ! shmsink socket-path=/tmp/gstreamer/raw-5 shm-size=20000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=10/1 ! shmsink socket-path=/tmp/gstreamer/raw-10 shm-size=20000000 wait-for-connection=false sync=false"]
        image: registry.gitlab.com/arm-research/smarter/edge-workloads/gstreamer:v1.0.0-rpi
        volumeMounts:
        - name: shared-data
          mountPath: /tmp/gstreamer
        env:
        - name: GST_DEBUG
          value: "1"
        resources:
          limits:
            smarter-devices/video0: 1
          requests:
            smarter-devices/video0: 1
      - name: gstreamer-rtsp-server
        ports:
          - containerPort: 8554
        imagePullPolicy: IfNotPresent
        command: ["./multicast-rtsp-server"]
        image: registry.gitlab.com/arm-research/smarter/edge-workloads/gstreamer:v1.0.0-rpi
        volumeMounts:
        - name: shared-data
          mountPath: /tmp/gstreamer
        env:
        - name: GST_DEBUG
          value: "2"
        resources:
          limits:
            smarter-devices/vchiq: 1
          requests:
            smarter-devices/vchiq: 1
      terminationGracePeriodSeconds: 15
      volumes:
      - name: shared-data
        emptyDir: {}

---

apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: gstreamer-jetson
  labels:
    k8s-app: gstreamer-jetson
spec:
  selector:
    matchLabels:
      name: gstreamer-jetson
  template:
    metadata:
      labels:
        name: gstreamer-jetson
    spec:
      nodeSelector:
        gstreamer: "enabled"
        smarter.nodetype: "jetson"
      tolerations:
      - key: "smarter.type"
        value: "edge"
        effect: "NoSchedule"
      hostname: gstreamer
      containers:
      - name: gstreamer-videotee
        imagePullPolicy: IfNotPresent
        command: ["/bin/bash"]
        args: ["-c", "gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=640,height=480,framerate=30/1 ! videoconvert ! video/x-raw,format=I420 ! tee name=30raw ! queue ! shmsink socket-path=/tmp/gstreamer/raw-30 shm-size=50000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=1/1 ! shmsink socket-path=/tmp/gstreamer/raw-1 shm-size=20000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=5/1 ! shmsink socket-path=/tmp/gstreamer/raw-5 shm-size=20000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=10/1 ! shmsink socket-path=/tmp/gstreamer/raw-10 shm-size=20000000 wait-for-connection=false sync=false"]
        image: registry.gitlab.com/arm-research/smarter/edge-workloads/gstreamer:v1.0.0-jetson
        volumeMounts:
        - name: shared-data
          mountPath: /tmp/gstreamer
        env:
        - name: GST_DEBUG
          value: "1"
        resources:
          limits:
            smarter-devices/video0: 1
          requests:
            smarter-devices/video0: 1
      - name: gstreamer-rtsp-server
        ports:
          - containerPort: 8554
        imagePullPolicy: IfNotPresent
        command: ["./multicast-rtsp-server"]
        image: registry.gitlab.com/arm-research/smarter/edge-workloads/gstreamer:v1.0.0-jetson
        volumeMounts:
        - name: shared-data
          mountPath: /tmp/gstreamer
        env:
        - name: GST_DEBUG
          value: "2"
      terminationGracePeriodSeconds: 15
      volumes:
      - name: shared-data
        emptyDir: {}

---

apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: gstreamer
  labels:
    k8s-app: gstreamer
spec:
  selector:
    matchLabels:
      name: gstreamer
  template:
    metadata:
      labels:
        name: gstreamer
    spec:
      nodeSelector:
        gstreamer: "enabled"
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: smarter.nodetype
                operator: NotIn
                values:
                - raspberrypi4
                - jetson
      tolerations:
      - key: "smarter.type"
        value: "edge"
        effect: "NoSchedule"
      hostname: gstreamer
      containers:
      - name: gstreamer-videotee
        imagePullPolicy: IfNotPresent
        command: ["/bin/bash"]
        args: ["-c", "gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=640,height=480,framerate=30/1 ! videoconvert ! video/x-raw,format=I420 ! tee name=30raw ! queue ! shmsink socket-path=/tmp/gstreamer/raw-30 shm-size=100000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=1/1 ! shmsink socket-path=/tmp/gstreamer/raw-1 shm-size=20000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=5/1 ! shmsink socket-path=/tmp/gstreamer/raw-5 shm-size=20000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=10/1 ! shmsink socket-path=/tmp/gstreamer/raw-10 shm-size=20000000 wait-for-connection=false sync=false"]
        image: registry.gitlab.com/arm-research/smarter/edge-workloads/gstreamer:v1.0.0
        volumeMounts:
        - name: shared-data
          mountPath: /tmp/gstreamer
        env:
        - name: GST_DEBUG
          value: "1"
        resources:
          limits:
            smarter-devices/video0: 1
          requests:
            smarter-devices/video0: 1
      - name: gstreamer-rtsp-server
        ports:
          - containerPort: 8554
        imagePullPolicy: IfNotPresent
        command: ["./multicast-rtsp-server"]
        image: registry.gitlab.com/arm-research/smarter/edge-workloads/gstreamer:v1.0.0
        volumeMounts:
        - name: shared-data
          mountPath: /tmp/gstreamer
        env:
        - name: GST_DEBUG
          value: "3"
      terminationGracePeriodSeconds: 15
      volumes:
      - name: shared-data
        emptyDir: {}