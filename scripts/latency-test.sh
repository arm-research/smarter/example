#!/bin/bash

set -e

gitroot=$(git rev-parse --show-toplevel)

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac
printf "Running on ${machine}\n"

if [ $machine == "Mac" ]; then
    DATE_CMD=gdate
else
    DATE_CMD=date
fi

if [ -z $1 ]; then
    reps=5
else
    reps=$1
fi
printf "Collecting latency data $reps times per category\n"


if ! { [ -z $CLOUD_KUBECONFIG ] && [ -z $EDGE_KUBECONFIG ] && [ -z $SMARTER_CLOUD_IP ]; }; then
  : do printf "Please set all of CLOUD_KUBECONFIG, EDGE_KUBECONFIG, and SMARTER_CLOUD_IP"
fi

nodenames=$(kubectl --kubeconfig $EDGE_KUBECONFIG get nodes -o wide | grep Ready | awk '{print $1}')

printf "Deploying echo server to cloud cluster\n"
kubectl --kubeconfig $CLOUD_KUBECONFIG apply -f $gitroot/yaml/echo-server
echopod=$(kubectl --kubeconfig $CLOUD_KUBECONFIG get pods | grep "echo-server" | awk '{print $1}')
sleep 5
printf "Deployed pod $echopod\n"

printf "Deploying latency test to edge\n"
envsubst < $gitroot/yaml/latency-test/latency-test-ds.yaml | kubectl --kubeconfig $EDGE_KUBECONFIG apply -f -

touch info.dat
echo "Device NoImage IfNotPresent Always" > info.dat
for HOST in $nodenames; do
    echo -n "$(kubectl --kubeconfig $EDGE_KUBECONFIG get nodes --show-labels | grep $HOST | grep -o 'smarter.nodetype=[^,]*' | sed 's/smarter.nodetype=//g')" >> info.dat
    for VAR in "NoImage" "IfNotPresent" "Always"; do
        if [ $VAR == "NoImage" ]; then
            export PULL_POLICY="IfNotPresent"
        else
            export PULL_POLICY=$VAR
        fi

        i="0"
        total="0"
        while [ $i -lt $reps ]; do
            if [ $VAR == "NoImage" ]; then
                kubectl --kubeconfig $EDGE_KUBECONFIG label node $HOST latency-test-
                ssh -t ubuntu@$HOST docker rmi curlimages/curl:7.70.0 || true &>/dev/null
            fi
            requests=$(kubectl --kubeconfig $CLOUD_KUBECONFIG logs $echopod | wc -l)            
            start=`$DATE_CMD +%s`
            kubectl --kubeconfig $EDGE_KUBECONFIG label node $HOST latency-test=enabled

            while [ $(kubectl --kubeconfig $CLOUD_KUBECONFIG logs $echopod | wc -l) -eq $requests ]; do
                printf "."
                sleep 1
            done

            cloudtimestamp=$(kubectl --kubeconfig $CLOUD_KUBECONFIG logs $echopod | tail -1 | awk '{print $2}')
            end=$($DATE_CMD -d "$cloudtimestamp" "+%s")
            diffsec=`echo ${end} - ${start} - 18000 | bc -l`
            printf "\nRuntime on $HOST for $VAR: ${diffsec}\n"
            total=$[$total+${diffsec}]

            kubectl --kubeconfig $EDGE_KUBECONFIG label node $HOST latency-test-
            while [ $(kubectl --kubeconfig $EDGE_KUBECONFIG get pods -o wide | grep latency-test | grep -c $HOST) -gt 0 ]; do
                sleep 1
            done
            i=$[$i+1]
        done
        average=`echo "scale=2; ${total} / $reps" | bc -l`
        printf "Average latency: $average\n"
        echo -n " $average" >> info.dat
    done
    echo "" >> info.dat
done

printf "Generating plot...\n"
gnuplot $gitroot/scripts/create-plot.txt

printf "Cleaning up\n"
kubectl --kubeconfig $EDGE_KUBECONFIG delete -f $gitroot/yaml/latency-test/latency-test-ds.yaml
exit 0




