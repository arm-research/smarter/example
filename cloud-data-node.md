# Overview
This document will help you install a Smarter k3s cloud data node  

# System requirements
## k3s cloud data node
* Local linux box, AWS EC2 VM instance or Google Cloud Platform GCE VM instance
* OS: Ubuntu 18.04
* Architecture: amd64
* CPU: at least 4vcpu
* RAM: At least 8GB
* Storage: At least 20GB

## dev machine
* User's desktop, capable of ssh'ing to the k3s cloud data node

## Network topology
* The k3s master and the dev machine both need access to the Internet.
* The dev machine needs to be able to `ssh` and `scp` into the k3s master.
* The k3s master needs to have ports 6443(k3s), 80(http), 443(https), and 30224(fluentd) opened in its firewall configuration.

# Setting k3s cloud data node up
Run the following commands on the k3s cloud data node:
Retrieve the scripts:
```
git clone https://gitlab.com/arm-research/smarter/example.git
```

Export the following names in your env on your dev machine:
```bash
export MY_EMAIL=<A_VALID_EMAIL>
```

**NOTE:** The user needs `sudo` privileges and might need to input password.
```
cd example
./scripts/cloud-data-setup.sh
```
The script will install a k3s master to manage cloud data services, helm to manage installation,
and then install InfluxDB for data storage and Grafana for visualization.
At this point, the k3s data node is running!

To fetch your grafana password for the admin user, in your cluster run:
```bash
kubectl get secret grafana-credentials -o jsonpath='{.data.admin-password}' | base64 --decode
```

To fetch your kibana/elasticsearch password for the elastic default user, in your cluster run:
```bash
kubectl get secret elastic-credentials -o jsonpath='{.data.password}' | base64 --decode
```

# **WARNING**

These scripts currently install persistent volumes via local-path. As such, this example is designed for a single node k3s cluster.

If you run this cloud portion locally (not AWS or GCE), self-signed certificates will be issued for your kibana and grafana domains. You must tell your browser to accept these certificates to view the sites.

# Firewall

See `Network topology` section above.
On AWS, you will need to do this by editing the security group policy and adding an inbound rule.

# DNS

Because we are using an ingress module, you will need to have DNS setup for services you want to access externally (like grafana).
By default we are using the nip.io wildcard domain which maps IP addresses to a host but allows for wildcard prefixes (like
what the ingress module needs).  If you'd like to use a custom domain, search through the yaml files and replace our defaults
with your host.

# NOTE: Let's Encypt Latency

We have noticed variable latency on let's encypt providing certificates to the configuration.  While in most cases
this should only be a matter of minutes, it can be up to an hour during which you won't be able to access the web
interfaces for grafana and kibana.  If you want to check to make sure the access is based on waiting for certificates
run
```bash
kubectl get certificates
```
and verify whether the kibana-tls-cert and grafana-tls-cert are present and marked as READY (which should mean 
let's encrypt has provided the certificates).  If this is false, then wait until they swap to True before attempting
to access the web interfaces.

