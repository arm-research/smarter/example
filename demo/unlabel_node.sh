kubectl label node ${NODE_TO_USE} image-detector-
kubectl label node ${NODE_TO_USE} audio-client-
kubectl label node ${NODE_TO_USE} fluent-bit-
kubectl label node ${NODE_TO_USE} pulse-
kubectl label node ${NODE_TO_USE} netdata-
kubectl label node ${NODE_TO_USE} triton-

if kubectl get node ${NODE_TO_USE} --show-labels | grep -q smarter.nodetype=jetson; then
    echo "Jetson"
else
    kubectl label node ${NODE_TO_USE} ambient-sound-classifier-
fi

