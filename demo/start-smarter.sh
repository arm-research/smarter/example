#!/bin/bash

kubectl apply -f ../yaml/smarter-cni/smartercni_ds.yaml
kubectl apply -f ../yaml/smarter-dns/smarterdns_ds_docker.yaml
kubectl apply -f ../yaml/smarter-device-manager/smarter-device-manager-k3s.yaml
exit

